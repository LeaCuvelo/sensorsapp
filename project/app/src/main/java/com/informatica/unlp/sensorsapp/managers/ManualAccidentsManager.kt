package com.informatica.unlp.sensorsapp.managers

interface ManualAccidentsManager {

    fun checkLatestManualAccident()
    fun uncheckLatestManualAccident()
    fun setLatestManualAccidentTypeValue(type : String)
    fun getLatetestAccident() : Boolean
    fun getLatetestAccidentType() : String


}