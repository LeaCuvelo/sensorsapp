package com.informatica.unlp.sensorsapp.db

import androidx.room.*
import com.informatica.unlp.sensorsapp.model.SensorMeasure

@Dao
interface SensorMeasureDao {

    @Insert
    fun insert(sensorMeasure: SensorMeasure)

    @Update
    fun update(sensorMeasure: SensorMeasure)

    @Delete
    fun delete(sensorMeasure: SensorMeasure)


    @Query("SELECT * FROM sensor_measure_table WHERE sessionId = :id")
    fun getAllMeasuresWithSessionId(id: Int): List<SensorMeasure>
}