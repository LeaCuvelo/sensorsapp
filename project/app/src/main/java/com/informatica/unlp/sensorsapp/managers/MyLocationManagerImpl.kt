package com.informatica.unlp.sensorsapp.managers

import android.content.Context
import android.location.Location
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationRequest



class MyLocationManagerImpl(var mContext: Context) : MyLocationManager
{
    private lateinit var locationCallback: LocationCallback
    private var mSettingsClient: SettingsClient? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null

    private var latestLocationValue : Location? = null
    private var latestLocationFlagValueToCalculate : Location ? = null
    private var latestLocationFlagValueToCalculateOneThousand : Location ? = null

    private var isFirstTime = true
    private lateinit var locationRequest: LocationRequest

    private var latestKnownSpeed : Double = 0.0
    private var latestKnownSpeedSum: Double = 0.0
    private var latestKnownSpeedSumSamples: Int = 0
    private var latestTransitabilityValue: Double = 0.0

    private var fusedLocationProviderClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext)

    private fun setupLocationServices() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext)
        mSettingsClient = LocationServices.getSettingsClient(mContext)
        setupLocationCallbackChanges()
    }

    private fun setupLocationCallbackChanges() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    latestLocationValue = location
                    latestKnownSpeed = convertInKilometersPerHs(location.speed)
                    if (isFirstTime){
                        latestLocationFlagValueToCalculate = location
                        latestLocationFlagValueToCalculateOneThousand = location
                        isFirstTime = false
                    }
                }
            }
        }
    }

    /**
     * speed is in meters per second
     */
    private fun convertInKilometersPerHs(speed: Float): Double {
        return speed * 3.6
    }

    override fun startLocationUpdates() {

        setupLocationServices()

        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 3000
        locationRequest.smallestDisplacement = 10f
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest)
        mLocationSettingsRequest = builder.build()
    }

    override fun getLatestLocation() = latestLocationValue

    override fun isOneHundredMetersTraveled(): Boolean {
        val distance = FloatArray(2)

        if(latestLocationValue?.latitude != null && latestLocationFlagValueToCalculate?.latitude != null){
            Location.distanceBetween(
                latestLocationValue!!.latitude, latestLocationValue!!.longitude,
                latestLocationFlagValueToCalculate!!.latitude, latestLocationFlagValueToCalculate!!.longitude, distance)
        }
        else
            return false

        if ( distance[0] >= 100 ){
            latestLocationFlagValueToCalculate = latestLocationValue
            latestKnownSpeedSum += latestKnownSpeed
            latestKnownSpeedSumSamples++
        }

        return distance[0] >= 100
    }

    override fun isOneThousandMetersTraveled(): Boolean {
        val distance = FloatArray(2)

        if(latestLocationValue?.latitude != null && latestLocationFlagValueToCalculateOneThousand?.latitude != null){
            Location.distanceBetween(
                latestLocationValue!!.latitude, latestLocationValue!!.longitude,
                latestLocationFlagValueToCalculateOneThousand!!.latitude, latestLocationFlagValueToCalculateOneThousand!!.longitude, distance)
        }
        else
            return false

        if ( distance[0] >= 1000 ){
            latestLocationFlagValueToCalculateOneThousand = latestLocationValue
            latestTransitabilityValue = latestKnownSpeedSum / latestKnownSpeedSumSamples
            latestKnownSpeedSum = 0.0
            latestKnownSpeedSumSamples = 0

        }

        return distance[0] >= 1000
    }

    override fun getLatestKnownSpeed() = latestKnownSpeed

    override fun getLatestKnownTransitabilityValue() = latestTransitabilityValue


}