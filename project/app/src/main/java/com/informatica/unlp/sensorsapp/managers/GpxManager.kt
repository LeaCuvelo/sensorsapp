package com.informatica.unlp.sensorsapp.managers

import com.informatica.unlp.sensorsapp.model.SensorMeasure

interface GpxManager {

    fun generateGpxFile(sensorMeasureList: List<SensorMeasure>)


}