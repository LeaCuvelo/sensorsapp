package com.informatica.unlp.sensorsapp.common

import android.os.Build
import android.os.Environment
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

val START_BUTTON_TEXT = "Comenzar"
val PAUSE_BUTTON_TEXT = "Pausa"
val RESUME_BUTTON_TEXT = "Continuar"
val SENSE_STATE_TEXT = "Censar"
val SENSE_RUNNING_STATE_TEXT = "Censando"

fun isExternalStorageWritable() = Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED

fun saveCurrentTime():String {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
        return current.format(formatter)
    } else {
        val date = Date()
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        return formatter.format(date)
    }
}