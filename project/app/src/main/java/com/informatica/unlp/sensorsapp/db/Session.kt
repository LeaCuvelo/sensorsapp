package com.informatica.unlp.sensorsapp.db

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "session_table")
data class Session(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    var sessionName:String
    //@Embedded(prefix = "sensor_")
    //var measure: SensorMeasure
)