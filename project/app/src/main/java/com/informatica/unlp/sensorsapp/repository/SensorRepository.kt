package com.informatica.unlp.sensorsapp.repository

interface SensorRepository {

    fun insertSession()
    fun insertSensorMeasure()
    fun writeInFile()
}