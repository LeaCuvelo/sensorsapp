package com.informatica.unlp.sensorsapp.viewmodels

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

class ViewModelScope : CoroutineScope {
    inline operator fun invoke(block: ViewModelScope.() -> Unit) = block(this)
    private val job: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
}