package com.informatica.unlp.sensorsapp.db

import androidx.room.TypeConverter
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class DatabaseUtils {
}

class Converters {

    @TypeConverter
    fun fromTimestamp(value: String?): LocalDate? {
        return value?.let {
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            val dateTime = LocalDate.parse(value, formatter)
            return dateTime
        }
    }

    @TypeConverter
    fun dateToTimestamp(localDateTime: LocalDate): String? {
        return localDateTime.toString()
    }

}