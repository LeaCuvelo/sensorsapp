package com.informatica.unlp.sensorsapp.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

class RepositoryScope : CoroutineScope {
    inline operator fun invoke(block: RepositoryScope.() -> Unit) = block(this)
    private val job: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
}