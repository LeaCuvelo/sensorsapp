package com.informatica.unlp.sensorsapp

import android.app.Application
import com.informatica.unlp.sensorsapp.db.SensorDatabase
import com.informatica.unlp.sensorsapp.managers.*
import com.informatica.unlp.sensorsapp.repository.RepositoryScope
import com.informatica.unlp.sensorsapp.repository.SensorRepository
import com.informatica.unlp.sensorsapp.repository.SensorRepositoryImpl
import com.informatica.unlp.sensorsapp.viewmodels.ViewModelFactory
import com.informatica.unlp.sensorsapp.viewmodels.ViewModelScope
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class SensorsAppApplication() : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@SensorsAppApplication))
        bind() from singleton { SensorDatabase(instance()) }
        bind<MySensorManager>() with singleton { MySensorManagerImpl(instance()) }
        bind<MyLocationManager>() with singleton { MyLocationManagerImpl(instance()) }
        bind<ManualAccidentsManager>() with singleton { ManualAccidentsManagerImpl() }
        bind<GpxManager>() with singleton { GpxManagerImpl(instance()) }
        bind<SensorRepository>() with singleton {
            SensorRepositoryImpl(
                instance(),
                instance(),
                instance(),
                instance(),
                instance(),
                instance(),
                RepositoryScope()
            )
        }
        bind() from provider {
            ViewModelFactory(
                instance(),
                instance(),
                instance(),
                instance(),
                instance(),
                ViewModelScope()
            )
        }
    }
}
