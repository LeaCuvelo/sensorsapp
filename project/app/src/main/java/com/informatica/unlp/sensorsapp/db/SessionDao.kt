package com.informatica.unlp.sensorsapp.db

import androidx.lifecycle.LiveData
import androidx.room.*
import java.util.*

@Dao
interface SessionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     fun insert(session: Session)

    @Update
     fun update(session: Session)

    @Delete
     fun delete(session: Session)


    @Query("SELECT * FROM session_table WHERE id == :id")
    fun getSessionById(id: Int): Session

    @Query("SELECT * FROM session_table WHERE sessionName == :sessionName")
    fun getSessionByName(sessionName: String): LiveData<Session>

    @Query("SELECT * FROM session_table")
    fun getAllSessions(): LiveData<List<Session>>
}