package com.informatica.unlp.sensorsapp.model

import androidx.room.*
import com.google.android.gms.maps.model.LatLng
import com.informatica.unlp.sensorsapp.db.Session
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@Entity(
    tableName = "sensor_measure_table",
    foreignKeys = [
        ForeignKey(
            entity = Session::class,
            parentColumns = ["id"],
            childColumns = ["sessionId"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class SensorMeasure(
    val sessionId: Int,
    var accelerometerMeasure: Float = 0.00f,
    var magnetometerMeasure: Float = 0.00f,
    var gyroscopeMeasure: Float = 0.00f ,
    var lat: Double? = null,
    var lng: Double? = null,
    var time: String? = null,
    val accidentType: String?,
    var manualAccident: Boolean
)
{
    @PrimaryKey(autoGenerate = true)
    var measureId: Int = 0
}

