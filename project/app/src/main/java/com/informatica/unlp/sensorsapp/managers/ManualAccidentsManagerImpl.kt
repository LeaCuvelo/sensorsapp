package com.informatica.unlp.sensorsapp.managers

class ManualAccidentsManagerImpl : ManualAccidentsManager {


    var latestManualAccident : Boolean = false
    var latestManualAccidentType : String = ""


    override fun checkLatestManualAccident() {
        latestManualAccident = true
    }

    override fun uncheckLatestManualAccident() {
        latestManualAccident = false
    }

    override fun setLatestManualAccidentTypeValue(type : String){
        latestManualAccidentType = type
    }

    override fun getLatetestAccident() : Boolean{
        return latestManualAccident
    }

    override fun getLatetestAccidentType(): String {
        return latestManualAccidentType
    }



}