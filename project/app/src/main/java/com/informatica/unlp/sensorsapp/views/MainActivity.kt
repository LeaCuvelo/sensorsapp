package com.informatica.unlp.sensorsapp.views

import android.Manifest
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.informatica.unlp.sensorsapp.R
import com.informatica.unlp.sensorsapp.databinding.ActivityMainBinding
import com.informatica.unlp.sensorsapp.viewmodels.MainViewModel
import com.informatica.unlp.sensorsapp.viewmodels.ViewModelFactory
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance

private const val PermissionsRequestResult = 0

class MainActivity : AppCompatActivity(), KodeinAware, OnMapReadyCallback {


    //region LocationProperties
    private lateinit var locationCallback: LocationCallback
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private lateinit var locationRequest: LocationRequest
    private var mSettingsClient: SettingsClient? = null
    private lateinit var mMap: GoogleMap
    private var mLastLocation: Location? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    //endregion LocationProperties

    //region ViewModelProperties
    private lateinit var mViewmodel: MainViewModel
    private val mViewModelFactory: ViewModelFactory by instance()
    private lateinit var binding: ActivityMainBinding
    override val kodein by closestKodein()
    //endregion ViewModelProperties

    //region LifeCycleMethods
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewmodel = ViewModelProvider(this, mViewModelFactory).get(MainViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewmodel = mViewmodel
        binding.lifecycleOwner = this@MainActivity

        setupPermissions()
        setupLocationServices()

    }

    //TODO desuscribirse como en el proyecto LocationUpdates
    override fun onResume() {
        super.onResume()
        startLocationUpdates()
        startLocationUpdatesNew()
        manualAccidentToastHandler()
        gpxNotifier()
    }
    //endregion LifeCycleMethods

    //region PermissionsMethods
    private fun setupPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestFineLocationPermission()
        }
    }

    private fun requestFineLocationPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            PermissionsRequestResult
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {


        when (requestCode) {
            PermissionsRequestResult -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

                } else {
                }
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }
    //endregion PermissionsMethods

    //region LocationMethods
    private fun setupLocationServices() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        mSettingsClient = LocationServices.getSettingsClient(this)
        setupLocationCallbackChanges()
    }

    private fun setupLocationCallbackChanges() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    mLastLocation = location
                    updateMapFragment()
                }
            }
        }
    }

    private fun updateMapFragment() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val lastLocation = LatLng(mLastLocation!!.latitude, mLastLocation!!.longitude)
        mMap.addMarker(MarkerOptions().position(lastLocation).title("Marker in current Location"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastLocation, 20.0f))
    }

    private fun startLocationUpdates() {
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 3000
        locationRequest.smallestDisplacement = 10f
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest)
        mLocationSettingsRequest = builder.build()
    }

    private fun startLocationUpdatesNew() {
        mSettingsClient!!.checkLocationSettings(mLocationSettingsRequest)
            .addOnSuccessListener(this, OnSuccessListener<LocationSettingsResponse> {
                Log.i("-----", "All location settings are satisfied.")

                fusedLocationProviderClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.myLooper()
                )
            })
            .addOnFailureListener(this, OnFailureListener { e ->
                val statusCode = (e as ApiException).statusCode
                when (statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        Log.i(
                            "***",
                            "Location settings are not satisfied. Attempting to upgrade " + "location settings "
                        )
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the
                            // result in onActivityResult().
                            val rae = e as ResolvableApiException
                            rae.startResolutionForResult(
                                this@MainActivity,
                                PermissionsRequestResult
                            )
                        } catch (sie: IntentSender.SendIntentException) {
                            Log.i("***", "PendingIntent unable to execute request.")
                        }

                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        val errorMessage =
                            "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
                        Log.e("ERROR!", errorMessage)
                        Toast.makeText(this@MainActivity, errorMessage, Toast.LENGTH_LONG).show()
                    }
                }
            })
    }

    private fun manualAccidentToastHandler() {
        mViewmodel.slipperyAccidentClick.observe(this, Observer {
            if (it) {
                Toast.makeText(this, "Camino Resbaladizo", Toast.LENGTH_SHORT).show()
                mViewmodel.slipperyAccidentClick.value = false
            }
        })

        mViewmodel.animalAccidentClick.observe(this, Observer {
            if (it) {
                Toast.makeText(this, "Animal Suelto", Toast.LENGTH_SHORT).show()
                mViewmodel.animalAccidentClick.value = false
            }
        })

        mViewmodel.alertAccidentClick.observe(this, Observer {
            if (it) {
                Toast.makeText(this, "Alerta", Toast.LENGTH_SHORT).show()
                mViewmodel.alertAccidentClick.value = false
            }
        })
        mViewmodel.sewerAccidentClick.observe(this, Observer {
            if (it) {
                Toast.makeText(this, "Alcantarilla Encontrada", Toast.LENGTH_SHORT).show()
                mViewmodel.sewerAccidentClick.value = false
            }
        })
    }

    private fun gpxNotifier() {
        mViewmodel.gpxCreated.observe(this, Observer {
            if (it) {
                Toast.makeText(this, "Gpx Creado", Toast.LENGTH_SHORT).show()
                mViewmodel.resetGpxValue()
            }
        })
    }
    //endregion LocationMethods
}





