package com.informatica.unlp.sensorsapp.viewmodels

import android.location.Location
import android.util.Log
import android.view.View
import androidx.lifecycle.*
import com.informatica.unlp.sensorsapp.common.*
import com.informatica.unlp.sensorsapp.managers.GpxManager
import com.informatica.unlp.sensorsapp.managers.ManualAccidentsManager
import com.informatica.unlp.sensorsapp.managers.MyLocationManager
import com.informatica.unlp.sensorsapp.managers.MySensorManager
import com.informatica.unlp.sensorsapp.model.SensorMeasure
import com.informatica.unlp.sensorsapp.repository.SensorRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate


class MainViewModel(
    private val mySensorManager: MySensorManager,
    private val myLocationManager: MyLocationManager,
    private val myGpxManager: GpxManager,
    private val mSensorRepository: SensorRepository,
    private val mManualAccidentsManager: ManualAccidentsManager
) : ViewModel() {


    private lateinit var saveMeasureTimer: Timer

    private var _stopIsVisible = MutableLiveData<Int>()
    val isVisible: LiveData<Int> get() = _stopIsVisible

    private var _startButtonText = MutableLiveData<String>()
    val startButtonText: LiveData<String> get() = _startButtonText

    private var _stateSenseText = MutableLiveData<String>()
    val stateSenseText: LiveData<String> get() = _stateSenseText

    private val _gyroscopeAverange = MutableLiveData<String>()
    val gyroscopeAverange: LiveData<String> get() = _gyroscopeAverange

    private val _magnetometerAverange = MutableLiveData<String>()
    val magnetometerAverange: LiveData<String> get() = _magnetometerAverange

    private val _accelerometerAverange = MutableLiveData<String>()
    val accelerometerAverange: LiveData<String> get() = _accelerometerAverange

    private val _accelerometerMaxValue = MutableLiveData<String>()
    val accelerometerMaxValue: LiveData<String> get() = _accelerometerMaxValue

    private val _gyroscopeMaxValue = MutableLiveData<String>()
    val gyroscopeMaxValue: LiveData<String> get() = _gyroscopeMaxValue

    private val _magnetometerMaxValue = MutableLiveData<String>()
    val magnetometerMaxValue: LiveData<String> get() = _magnetometerMaxValue

    private var transitabilityValue: Double = 0.0

    private val _transitabilityValueBind = MutableLiveData<String>()
    val transitabilityValueBind: LiveData<String> get() = _transitabilityValueBind

    //Accident buttons
    var slipperyAccidentClick = MutableLiveData<Boolean>()

    var animalAccidentClick = MutableLiveData<Boolean>()

    var alertAccidentClick = MutableLiveData<Boolean>()

    var sewerAccidentClick = MutableLiveData<Boolean>()

    private var _gpxCreated = MutableLiveData<Boolean>()
    val gpxCreated: LiveData<Boolean> get() = _gpxCreated

    private var _accidentEnabled = MutableLiveData<Boolean>()
    val accidentEnabled: LiveData<Boolean> get() = _accidentEnabled


    init {
        mySensorManager.initializeAllSensors()
        myLocationManager.startLocationUpdates()

        mySensorManager._magnetometerAverage.observeForever {
            _magnetometerAverange.value = it
        }

        mySensorManager._gyroscopeAverage.observeForever {
            _gyroscopeAverange.value = it
        }

        mySensorManager._accelerometerAverage.observeForever {
            _accelerometerAverange.value = it
        }

        mySensorManager._magnetometerMaxValue.observeForever {
            _magnetometerMaxValue.value = it
        }

        mySensorManager._gyroscopeMaxValue.observeForever {
            _gyroscopeMaxValue.value = it
        }

        mySensorManager._accelerometerMaxValue.observeForever {
            _accelerometerMaxValue.value = it
        }


        _stopIsVisible.value = View.GONE
        _startButtonText.value = START_BUTTON_TEXT
        _stateSenseText.value = SENSE_STATE_TEXT


        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                _transitabilityValueBind.value = "0.0"
            }
        }

        _gpxCreated.value = false
        _accidentEnabled.value = false
        slipperyAccidentClick.value = false
        alertAccidentClick.value = false
        animalAccidentClick.value = false
        sewerAccidentClick.value = false

    }

    // Sensors Methods
    fun startSense() {

        if (_stopIsVisible.value == View.GONE && startButtonText.value == START_BUTTON_TEXT) {
            showStopButton()
            _accidentEnabled.value = true
            changeToPauseText()
            changeSenseToRunStateText()
            viewModelScope.launch {
                withContext(Dispatchers.IO) {
                    startPeriodicWriteInDB()
                }
            }
        } else if (_stopIsVisible.value == View.VISIBLE && startButtonText.value == PAUSE_BUTTON_TEXT) {
            changeToResumeText()
            changeSenseStateText()
            pauseSense()
        } else if (_stopIsVisible.value == View.VISIBLE && startButtonText.value == RESUME_BUTTON_TEXT) {
            changeToPauseText()
            changeSenseToRunStateText()
            resumeSense()
        }
    }

    private fun startPeriodicWriteInDB() {
        saveMeasureTimer = Timer()
        startTimer()
    }

    private fun startTimer() {
        mSensorRepository.insertSession()
        saveMeasureTimer.scheduleAtFixedRate(200, 200) {
            //TODO migrate to a services with interrupts
            if (myLocationManager.isOneHundredMetersTraveled()) {
                mySensorManager.calculateAverages()
                mSensorRepository.insertSensorMeasure()
            }
            if (myLocationManager.isOneThousandMetersTraveled()) {
                if (myLocationManager.getLatestKnownTransitabilityValue() > 80.0) {
                    transitabilityValue = 1.0
                } else {
                    transitabilityValue =
                        myLocationManager.getLatestKnownTransitabilityValue() * 1.25
                }
                viewModelScope.launch {
                    withContext(Dispatchers.Main) {
                        _transitabilityValueBind.value = transitabilityValue.toString()
                    }
                }
            }
        }
    }

    fun stopTimer() {
        saveMeasureTimer.cancel()
        mSensorRepository.writeInFile()
        stopSenseUI()
    }

    fun pauseSense() {
        mySensorManager.stopListenerSensors()
        saveMeasureTimer.cancel()

    }

    fun resumeSense() {
        mySensorManager.resumeListenerSensors()
        saveMeasureTimer = Timer()
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                saveMeasureTimer.scheduleAtFixedRate(200, 2000) {
                    mSensorRepository.insertSensorMeasure()
                }
            }
        }

    }

    fun stopSenseUI() {
        hideStopButton()
        changeToStartText()
        changeSenseStateText()
        _accidentEnabled.value = false
        _gpxCreated.value = true
    }

    fun saveSense() {
        // mySensorManager.saveSense()
        //generateGpxFile()
        _gpxCreated.value = true

    }

     fun generateGpxFile(sensorMeasureList: List<SensorMeasure>) {
        myGpxManager.generateGpxFile(sensorMeasureList)//Todo Aca va la lista de SensorMeasure sacada de la sesion para que el GPX manager haga el archivo
    }

    fun slipperyAccident() {
        mManualAccidentsManager.checkLatestManualAccident()
        mManualAccidentsManager.setLatestManualAccidentTypeValue("Slippery")
        slipperyAccidentClick.value = true
    }

    fun animalAccident() {
        mManualAccidentsManager.checkLatestManualAccident()
        mManualAccidentsManager.setLatestManualAccidentTypeValue("Animal")
        animalAccidentClick.value = true
    }

    fun alertAccident() {
        mManualAccidentsManager.checkLatestManualAccident()
        mManualAccidentsManager.setLatestManualAccidentTypeValue("Alert")
        alertAccidentClick.value = true

    }

    fun sewerAccident() {
        mManualAccidentsManager.checkLatestManualAccident()
        mManualAccidentsManager.setLatestManualAccidentTypeValue("Sewer")
        sewerAccidentClick.value = true
    }

    fun resetGpxValue() {
        _gpxCreated.value = false
    }

    // END Sensors Methods


    // Location Methods

    private fun updateMapPosition(location: Location?) {


    }

    private fun updateCoordinatesValues(location: Location?) {

    }

    // END Location Methods


    // Lifecycle Methods

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME) //TODO use lifecycle events para subscribir y desuscribir el listener de sensores
    fun connectListener() {
        Log.d("MainViewModel", "!!!!!!")
    }

    private fun showStopButton() {
        _stopIsVisible.value = View.VISIBLE
    }

    private fun hideStopButton() {
        _stopIsVisible.value = View.GONE
    }

    private fun changeToStartText() {
        _startButtonText.value = START_BUTTON_TEXT
    }

    private fun changeToPauseText() {
        _startButtonText.value = PAUSE_BUTTON_TEXT
    }

    private fun changeSenseStateText() {
        _stateSenseText.value = SENSE_STATE_TEXT
    }

    private fun changeSenseToRunStateText() {
        _stateSenseText.value = SENSE_RUNNING_STATE_TEXT
    }

    private fun changeToResumeText() {
        _startButtonText.value = RESUME_BUTTON_TEXT
    }

    // END Lifecycle Methods
}