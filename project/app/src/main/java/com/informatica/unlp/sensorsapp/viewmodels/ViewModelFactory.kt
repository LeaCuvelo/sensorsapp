package com.informatica.unlp.sensorsapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.informatica.unlp.sensorsapp.managers.GpxManager
import com.informatica.unlp.sensorsapp.managers.ManualAccidentsManager
import com.informatica.unlp.sensorsapp.managers.MyLocationManager
import com.informatica.unlp.sensorsapp.managers.MySensorManager
import com.informatica.unlp.sensorsapp.repository.SensorRepository

class ViewModelFactory(
    private val mySensorManager: MySensorManager,
    private val myLocationManager: MyLocationManager,
    private val myGpxManager: GpxManager,
    private val mRepository: SensorRepository,
    private val mManualAccidentsManager: ManualAccidentsManager,
    private val viewModelScope: ViewModelScope
) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(
            mySensorManager,
            myLocationManager,
            myGpxManager,
            mRepository,
            mManualAccidentsManager
        ) as T
    }
}