package com.informatica.unlp.sensorsapp.managers

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.scheduleAtFixedRate

class MySensorManagerImpl(var mContext: Context) : MySensorManager, SensorEventListener {

    override val _accelerometerMaxValue: MutableLiveData<String> = MutableLiveData()
    override val _magnetometerMaxValue: MutableLiveData<String> = MutableLiveData()
    override val _gyroscopeMaxValue: MutableLiveData<String> = MutableLiveData()

    override val _gyroscopeAverage: MutableLiveData<String> = MutableLiveData()
    override val _magnetometerAverage: MutableLiveData<String> = MutableLiveData()
    override val _accelerometerAverage: MutableLiveData<String> = MutableLiveData()

    private lateinit var mSensorManager: SensorManager
    private var mSensorAccelerometer: Sensor? = null
    private var mSensorGyroscope: Sensor? = null
    private var mSensorMagnetometer: Sensor? = null

    private var lastGyroscopeZValue: Float = 0f
    private var lastMagnetometerValue: Float = 0f
    private var lastAccelerometerZValue: Float = 0f

    private var lastGyroscopeZValueSum: Float = 0f
    private var lastMagnetometerValueSum: Float = 0f
    private var lastAccelerometerValueSum: Float = 0f

    private var lastGyroscopeZValueAverageResult: Float = 0f
    private var lastMagnetometerValueSumAverageResult: Float = 0f
    private var lastAccelerometerValueSumAverageResult: Float = 0f

    private var lastGyroscopeZValueMaxValue: Float = 0f
    private var lastMagnetometerValueMaxValue: Float = 0f
    private var lastAccelerometerValueMaxValue: Float = 0f

    private var lastGyroscopeZValueSumSamples: Int = 0
    private var lastMagnetometerValueSumSamples: Int = 0
    private var lastAccelerometerValueSumSamples: Int = 0

    private lateinit var sumMeasureTimer: Timer
    private lateinit var calculateAverageTimer: Timer


    init {
        sumMeasureTimer = Timer()
        calculateAverageTimer = Timer()
    }


    private fun startSumMeasureTimer() {
        sumMeasureTimer.scheduleAtFixedRate(200, 200) {
            sumInAverageAcumulatorAndCalculateMaxValue()
        }
    }

    override fun calculateAverages(){
        lastAccelerometerValueSumAverageResult = lastAccelerometerValueSum / lastAccelerometerValueSumSamples
        lastAccelerometerValueSumSamples = 0
        lastAccelerometerValueSum = 0F

        lastGyroscopeZValueAverageResult = lastGyroscopeZValueMaxValue / lastGyroscopeZValueSumSamples
        lastGyroscopeZValueSumSamples = 0
        lastGyroscopeZValueMaxValue = 0F

        lastMagnetometerValueSumAverageResult = lastMagnetometerValueSum / lastMagnetometerValueSumSamples
        lastMagnetometerValueSumSamples = 0
        lastMagnetometerValueSum = 0F

        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                _accelerometerAverage.value = lastAccelerometerValueSumAverageResult.toString()
                _gyroscopeAverage.value = lastGyroscopeZValueAverageResult.toString()
                _magnetometerAverage.value = lastMagnetometerValueSumAverageResult.toString()
                _accelerometerMaxValue.value = lastAccelerometerValueMaxValue.toString()
                _gyroscopeMaxValue.value = lastGyroscopeZValueMaxValue.toString()
                _magnetometerMaxValue.value = lastMagnetometerValueMaxValue.toString()
            }
        }


    }


    private fun sumInAverageAcumulatorAndCalculateMaxValue(){
        lastGyroscopeZValueSum  += lastGyroscopeZValue
        lastGyroscopeZValueSumSamples++
        if(lastGyroscopeZValue > lastGyroscopeZValueMaxValue)
            lastGyroscopeZValueMaxValue = lastGyroscopeZValue

        lastAccelerometerValueSum  += lastAccelerometerZValue
        lastAccelerometerValueSumSamples++
        if(lastAccelerometerZValue > lastAccelerometerValueMaxValue)
            lastAccelerometerValueMaxValue = lastAccelerometerZValue

        lastMagnetometerValueSum  += lastMagnetometerValue
        lastMagnetometerValueSumSamples++
        if(lastMagnetometerValue > lastMagnetometerValueMaxValue)
            lastMagnetometerValueMaxValue = lastMagnetometerValue

    }

    private fun registerListener() {
        mSensorManager.registerListener(this, mSensorAccelerometer, SensorManager.SENSOR_DELAY_UI)
        mSensorManager.registerListener(this, mSensorGyroscope, SensorManager.SENSOR_DELAY_UI)
        mSensorManager.registerListener(this, mSensorMagnetometer, SensorManager.SENSOR_DELAY_UI)
    }

    override fun initializeAllSensors() {
        mSensorManager = mContext.getSystemService(Context.SENSOR_SERVICE ) as SensorManager
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mSensorGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
        mSensorMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)

        registerListener()
        startSumMeasureTimer()
    }

    override fun stopListenerSensors(){
        mSensorManager.unregisterListener(this, mSensorAccelerometer)
        mSensorManager.unregisterListener(this, mSensorGyroscope)
        mSensorManager.unregisterListener(this, mSensorMagnetometer)
    }

    override fun resumeListenerSensors(){
        registerListener()
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }

    /*
    * a[0] = x
    * a[1] = y
    * a[2] = z
    * */
    override fun onSensorChanged(p0: SensorEvent?) {
        //Sensor change value
        var a = p0!!.values

        if (p0.sensor.type == Sensor.TYPE_ACCELEROMETER){
            lastAccelerometerZValue = a[2]
        }
        if (p0.sensor.type == Sensor.TYPE_GYROSCOPE){
            lastGyroscopeZValue = a[2]
        }
        if (p0.sensor.type == Sensor.TYPE_MAGNETIC_FIELD){
            lastMagnetometerValue = a[2]
        }
    }

    /**
      Return list of three sensors
       1 - accelerometerAverage in z axis
       2 - magnetometerAverage in z axis
       3 - gyroscopeAverage in z axis
    * */
    override fun retrieveLastValueSensors(): ArrayList<Float> {
        var sensorList = ArrayList<Float>()
        sensorList.add(lastMagnetometerValue)
        sensorList.add(lastMagnetometerValue)
        sensorList.add(lastGyroscopeZValue)

        return sensorList
    }

    /**
    Return list of three sensors averages
    1 - accelerometerAverage in z axis
    2 - magnetometerAverage in z axis
    3 - gyroscopeAverage in z axis
     * */
    override fun retrieveLastAverangeValueSensors(): ArrayList<Float> {
        var sensorListAverage = ArrayList<Float>()
        sensorListAverage.add(lastAccelerometerValueSumAverageResult)
        sensorListAverage.add(lastMagnetometerValueSumAverageResult)
        sensorListAverage.add(lastGyroscopeZValueAverageResult)

        return sensorListAverage
    }


}