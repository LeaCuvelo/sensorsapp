package com.informatica.unlp.sensorsapp.managers

import android.content.Context
import android.os.Environment
import com.informatica.unlp.sensorsapp.common.isExternalStorageWritable
import com.informatica.unlp.sensorsapp.model.SensorMeasure
import java.io.File

class GpxManagerImpl(context: Context) : GpxManager {
    private val mContext = context
    private val fileContent = StringBuilder()

    override fun generateGpxFile(sensorMeasureList: List<SensorMeasure>) {//TODO Definir tags
        if (isExternalStorageWritable()) {
            val fileGpx = File(mContext.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "gpx_file_1.txt")
            fileContent.append("<?xml version=\"1.0\"?><gpx version= \"1.1\" creator=\"sensorApp\">")
            for (sensorMeasure in sensorMeasureList) {
                fileContent.append("<wpt lat=" + '"' + sensorMeasure.lat + '"' + " lon=" + '"' + sensorMeasure.lng + '"' + ">" + "<time>" + sensorMeasure.time + "</time>" + "<ele>" + sensorMeasure.gyroscopeMeasure + "</ele>" + "<magvar>" + sensorMeasure.magnetometerMeasure + "</magvar>" + "<speed>" + sensorMeasure.accelerometerMeasure + "</speed>" + "</wpt>")
            }
            fileContent.append("</gpx>")
            fileGpx.writeText(fileContent.toString())
            println(fileContent.toString())
        } else
            print("File No created")
    }
}