package com.informatica.unlp.sensorsapp.managers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData


interface MySensorManager {

    val  _gyroscopeAverage: MutableLiveData<String>
    val gyroscopeAverage : LiveData<String> get()= _gyroscopeAverage

    val  _magnetometerAverage: MutableLiveData<String>
    val magnetometerAverage : LiveData<String> get()= _magnetometerAverage

    val  _accelerometerAverage: MutableLiveData<String>
    val accelerometerAverage : LiveData<String> get()= _accelerometerAverage

    val  _accelerometerMaxValue: MutableLiveData<String>
    val accelerometerMaxValue : LiveData<String> get()= _accelerometerMaxValue

    val  _magnetometerMaxValue: MutableLiveData<String>
    val magnetometerMaxValue : LiveData<String> get()= _magnetometerMaxValue

    val  _gyroscopeMaxValue: MutableLiveData<String>
    val gyroscopeMaxValue : LiveData<String> get()= _gyroscopeMaxValue

    fun initializeAllSensors()
    fun retrieveLastValueSensors(): ArrayList<Float>
    fun stopListenerSensors()
    fun resumeListenerSensors()
    fun retrieveLastAverangeValueSensors(): ArrayList<Float>
    fun calculateAverages()


}