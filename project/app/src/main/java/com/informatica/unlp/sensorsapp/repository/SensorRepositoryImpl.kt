package com.informatica.unlp.sensorsapp.repository

import android.content.Context
import android.os.Environment
import com.informatica.unlp.sensorsapp.common.isExternalStorageWritable
import com.informatica.unlp.sensorsapp.common.saveCurrentTime
import com.informatica.unlp.sensorsapp.db.SensorDatabase
import com.informatica.unlp.sensorsapp.db.Session
import com.informatica.unlp.sensorsapp.managers.GpxManager
import com.informatica.unlp.sensorsapp.managers.ManualAccidentsManager
import com.informatica.unlp.sensorsapp.managers.MyLocationManager
import com.informatica.unlp.sensorsapp.managers.MySensorManager
import com.informatica.unlp.sensorsapp.model.SensorMeasure
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File


class SensorRepositoryImpl(
    private var sensorDatabase: SensorDatabase,
    private var mContext: Context,
    private var mySensorManager: MySensorManager,
    private var myLocationManager: MyLocationManager,
    private var myManualAccidentsManager: ManualAccidentsManager,
    private var mGPXManager: GpxManager,
    private val repositoryScope: RepositoryScope
) : SensorRepository {

    private val fileContent = StringBuilder()
    override fun insertSession() {
        sensorDatabase.let { it.sessionDao().insert(Session(1, "session")) }
    }

    override fun insertSensorMeasure() {

        var isManual = false
        var accidentType = ""
        var sensorsValuesAverageList = mySensorManager.retrieveLastAverangeValueSensors()
        var latestLocation = myLocationManager.getLatestLocation()
        var latestSpeed = myLocationManager.getLatestKnownSpeed()
        if (myManualAccidentsManager.getLatetestAccident()) {
            isManual = true
            accidentType = myManualAccidentsManager.getLatetestAccidentType()
            myManualAccidentsManager.uncheckLatestManualAccident()
        }

        sensorDatabase.let {
            it.sensorMeasureDao().insert(
                SensorMeasure(
                    1,
                    sensorsValuesAverageList[0],
                    sensorsValuesAverageList[1],
                    sensorsValuesAverageList[2],
                    latestLocation?.latitude,
                    latestLocation?.longitude,
                    saveCurrentTime(),
                    accidentType,
                    isManual
                )
            )
        }
    }

    override fun writeInFile() {
        repositoryScope.launch {
            withContext(Dispatchers.IO) {
                val session: Session
                var measureList: List<SensorMeasure>
                session = sensorDatabase.sessionDao().getSessionById(1)
                measureList =
                    sensorDatabase.sensorMeasureDao().getAllMeasuresWithSessionId(session.id)
                alreadyGetMeasureList(measureList)
            }
        }
    }

    fun alreadyGetMeasureList(meassureList: List<SensorMeasure>) {
        if (isExternalStorageWritable()) {
            val file = File(
                mContext.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS),
                "dbTXT.txt"
            )
            fileContent.append(meassureList.toString())
            file.writeText(fileContent.toString())
            mGPXManager.generateGpxFile(meassureList)
            println(fileContent.toString())
        } else
            print("File No created")
    }
}