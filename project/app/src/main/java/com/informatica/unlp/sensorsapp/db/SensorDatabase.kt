package com.informatica.unlp.sensorsapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.informatica.unlp.sensorsapp.model.SensorMeasure

@Database(entities = arrayOf(Session::class, SensorMeasure::class), version = 1)
@TypeConverters(Converters::class)
abstract class SensorDatabase : RoomDatabase() {

    abstract fun sessionDao(): SessionDao
    abstract fun sensorMeasureDao(): SensorMeasureDao


    //singleton database instantiation
    companion object {
        private var instance: SensorDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                SensorDatabase::class.java, "sensor.db"
            )
                .build()
    }
}
