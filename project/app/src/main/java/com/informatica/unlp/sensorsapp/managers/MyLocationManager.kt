package com.informatica.unlp.sensorsapp.managers

import android.location.Location


interface MyLocationManager {

    fun startLocationUpdates()
    fun getLatestLocation(): Location?
    fun isOneHundredMetersTraveled(): Boolean
    fun isOneThousandMetersTraveled(): Boolean
    fun getLatestKnownSpeed(): Double
    fun getLatestKnownTransitabilityValue(): Double

}